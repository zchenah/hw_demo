# coding:utf8
import MySQLdb
import json
from dateutil.parser import parse as date_parse

__author__ = 'chin'


db = MySQLdb.connect(host='csz908.cse.ust.hk',
                     user='root',
                     passwd="swarm",
                     db="hw_demo",
                     port=3306,
                     charset='utf8')
cursor = db.cursor()
cursor.execute("SELECT VERSION()")
ver = cursor.fetchone()
print ver


def bulk_insert(sql, tuple_list):
    try:
        cursor = db.cursor()
        cursor.executemany(sql, tuple_list)
        db.commit()
    except Exception as e:
        print e

def grab_tweet(txt):
    t = json.loads(txt)
    return (t['id'], t['user']['id'], t['text'], date_parse(t['created_at']))

def main():
    sql = 'insert into tweet (tid, uid, text, created_at) values (%s, %s, %s, %s)'

    f = file('sample_10000','r')
    lst = []
    for line in f:
        lst.append(grab_tweet(line))
        if len(line)==1000:
            bulk_insert(sql, lst)
            lst = []
    return


if __name__ == '__main__':
    main()