# coding:utf-8
__author__ = 'chin'
import os.path
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy


ROOT = os.path.join(os.path.dirname(__file__), 'sqlite')
SQLITE_PATH = os.path.join(ROOT, 'hw_demo.sqlite')

DATABASE = 'sqlite:///'+SQLITE_PATH

dbhost = '1234567'
dbuser = ''
dbpass = ''
dbname = 'hw_demo'

DATABASE = 'mysql://' + dbuser + ':' + dbpass + '@' + dbhost + '/' +dbname

APP_NAME = 'hw_demo'
app = Flask(APP_NAME)
app.debug = True
app.secret_key = 'lalalala'
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE

db = SQLAlchemy(app)


def main():
    pass


if __name__ == '__main__':
    main()
