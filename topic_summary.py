# coding:utf8

import MySQLdb
import json
from dateutil.parser import parse as date_parse

__author__ = 'chin'


db = MySQLdb.connect(host='csz908.cse.ust.hk',
                     user='root',
                     passwd="swarm",
                     db="hw_demo",
                     port=3306,
                     charset='utf8')
cursor = db.cursor()
cursor.execute("SELECT VERSION()")
ver = cursor.fetchone()
print ver

__author__ = 'chin'

def get_keywords():
    cursor.execute( 'SET SESSION group_concat_max_len = 1000000;')
    sql = '''
    select hashtag, group_concat(distinct keyword order by keyword_weight desc separator ',')
                from hot_hashtag_keyword_200
                group by hashtag;'''
    cursor.execute(sql)
    r = cursor.fetchall()
    return r

def gen_score_table(tags):
    res = []
    for hashtag1, keywords1 in tags.items():
        res_row = []
        for hashtag2, keywords2 in tags.items():
            if hashtag1>hashtag2:
                common = keywords1&keywords2
                # print 'score of:', hashtag1, hashtag2
                bonus = 10 if h_in_ks(hashtag2,keywords1) or h_in_ks(hashtag1,keywords2) else 0
                res_row.append( (hashtag2, len(common)+bonus) )
        res.append( (hashtag1, res_row) )
    return res


def h_in_ks(hashtag, keywords):
    if len(hashtag)==1:
        hashtag = hashtag[0]
    if type(hashtag) is not str:
        # print 'type of', hashtag
        return h_in_ks(hashtag[0], keywords) or h_in_ks(hashtag[1:], keywords)
    return hashtag.lower() in (kw.lower() for kw in keywords) or any(hashtag.lower() in kw.lower() for kw in keywords)

def merge_tags(tags_list):
    KW_COUNT = 20
    res = dict( ((str(k),), set(v.split(',')[:KW_COUNT])) for k,v in tags_list)
    print res.keys()
    while True:
        scores_table = gen_score_table(res)
        maxtag1, maxtag2, max_score = None, None, 0
        for tag1, scores_row in scores_table:
            for tag2, score in scores_row:
                if score>max_score:
                    maxtag1, maxtag2 = tag1,tag2
                    max_score = score
        # print maxtag1, maxtag2, max_score
        if max_score and max_score>KW_COUNT/2:   #(len(res[maxtag1])+len(res[maxtag2]))/
            new_key = maxtag1+maxtag2
            res[new_key] = res[maxtag1]|res[maxtag2]
            res.pop(maxtag1)
            res.pop(maxtag2)
        else:
            break
    print res.keys()
    for i, t in enumerate(res.keys()):
        sql = 'insert into hashtag_group (hashtag, gid) values (%s, %s)'
        cursor.executemany(sql, ((v,i) for v in t))


def main():
    keywords_r = get_keywords()
    merge_tags(keywords_r)
    db.commit()
    return

if __name__ == '__main__':
    main()