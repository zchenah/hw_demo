# coding:utf-8

import re
import json
from flask import Flask
from flask import make_response, render_template, url_for, request, session, jsonify, redirect, g
# from sqlalchemy import func
from config import app, db
from collections import defaultdict

# from functools import wraps

# from models import *

def no_render_template(filename):
    make_response(open('templates/'+filename).read())


@app.route('/')
def index():
    return redirect('./event/')

@app.route('/event/')
def index_event():
    return render_template('index.html')

@app.route('/views/events/<int:event_id>')
def event_view(event_id):
    pass

@app.route('/simple/')
def hash():
    return render_template('simple.html')

@app.route('/combined/')
def hash_topic():
    return render_template('hash-topic.html')

@app.route('/about/')
def about():
    return render_template('about.html')


@app.route('/rest/keywords/<topic>')
def keywords(topic):
    keywords_by_topic = query_keywords(hashtag=topic)
    print keywords_by_topic[:3]
    return json.dumps(keywords_by_topic)


@app.route('/rest/hashtags')
def hashtags():
    hashtags_by_day = query_hashtags()
    print hashtags_by_day[:3]
    return json.dumps(hashtags_by_day)

@app.route('/rest/groups')
def groups():
    g = query_groups()
    return json.dumps( g )


@app.route('/rest/topics')
def topics():
    sql = '''select hashtag, period_start, cnt from  hot_hashtag_detail where cnt>=100 order by period_start'''
    r = db.engine.execute(sql).fetchall()

    all_hashes_set = set(h for h in query_g_hashes())
    print len(r)
    hashes = fill_with_0(r, all_hashes_set)
    hashes_dict = {}
    for h in hashes:
        hashes_dict[h['name']] = h['values']

    group_list = query_groups()
    res = {}
    for g in group_list:
        res[','.join(g['values'])] = [0.0,]*30
    for group in group_list:
        gname, ghashes = ','.join(group['values']) , group['values']
        for ghash in ghashes:
            hvalues = hashes_dict[ghash]
            gvalues = res[gname]
            print ghash, hvalues, gvalues
            for i in range(30):
                gvalues[i] += hvalues[i]

    res_list = []
    for k,v in res.items():
        res_list.append({'name':k, 'values':v})


    return json.dumps( res_list)

    # for gid, hash_list in g_dict.items():
    #     for hname in hash_list:
    #         for hash in hashes:
    #             if hname == hash['name']:
    #
    #                 pass
    #
    #
    # topics_by_day = query_topics()
    # # topics_by_topic=pivot_topic(topics_by_day)
    # return json.dumps(topics_by_day)



MOCK_DATA_LENGTH = 30
MOCK_EVENTS_COUNT = 10
RANDOM_INT_LENGTH = 100
MOCK_RANDOM_INT = [21 , 53 , 35 , 100 , 82 , 26 , 94 , 57 , 22 , 1 , 99 , 93 , 77 , 48 , 58 , 2 , 22 , 63 , 42 , 52 ,
                   67 , 41 , 29 , 4 , 46 , 4 , 45 , 95 , 64 , 44 , 92 , 92 , 9 , 56 , 85 , 5 , 98 , 78 , 58 , 37 , 88 ,
                   20 , 34 , 81 , 35 , 95 , 88 , 46 , 60 , 90 , 50 , 96 , 16 , 87 , 92 , 58 , 64 , 10 , 75 , 39 , 92 ,
                   77 , 30 , 5 , 78 , 18 , 91 , 6 , 89 , 54 , 42 , 34 , 85 , 52 , 3 , 32 , 85 , 21 , 9 , 13 , 65 , 66 ,
                   40 , 51 , 32 , 73 , 95 , 55 , 4 , 11 , 60 , 2 , 17 , 92 , 78 , 54 , 93 , 56 , 77 , 36 ]

def mock_all_eids():
    return range(MOCK_EVENTS_COUNT)
    pass

def mock_event_hotness(eid):
    begin = MOCK_RANDOM_INT[eid]
    res = [MOCK_RANDOM_INT[i*(eid**10)%RANDOM_INT_LENGTH] for i in range(MOCK_DATA_LENGTH)]
    begin_day,end_day = sorted([begin%MOCK_DATA_LENGTH-10, begin*begin%MOCK_DATA_LENGTH+10])
    for i in range(MOCK_DATA_LENGTH):
        if i < begin_day or i > end_day:
            res[i] = 0
    return res


def mock_event_weights(eid):
    all_hotness = [mock_event_hotness(i) for i in range(MOCK_EVENTS_COUNT)]
    sum_hotness = map(sum, zip(*all_hotness))
    return map(lambda x,y: y and float(x)/y, mock_event_hotness(eid), sum_hotness)

def mock_event_tids(eid):
    TOPIC_LIMIT = 16
    topics_count = MOCK_RANDOM_INT[(eid*142857)%RANDOM_INT_LENGTH] % TOPIC_LIMIT + 1
    tid_list = [ (i*eid**49)%RANDOM_INT_LENGTH for i in range(1, topics_count+1)]
    print tid_list
    tid_set = set(tid_list)
    print tid_set
    unsort = [MOCK_RANDOM_INT[ui] for ui in tid_set]
    return [tid for tid in sorted(unsort)]

def mock_topic_hotness(tid, eid):
    eh = mock_event_hotness(eid)
    return [ (h*(MOCK_RANDOM_INT[(tid**eid)%RANDOM_INT_LENGTH]))%RANDOM_INT_LENGTH for h in eh]

def mock_topic_keywords(tid):
    word_list = ['asdf', 'fda', 'ads', 'sdsdsds', 'sadfads', 'sadfd']
    begin = tid%(RANDOM_INT_LENGTH/2)
    weight_list = MOCK_RANDOM_INT[begin: begin+len(word_list)]
    return dict(zip(word_list, weight_list))
    # eh = mock_event_hotness(eid)

def topic_keywords(tid):
    sql = 'select keyword, keyword_weight from topic_hashtag_new where topic_id=%s order by keyword_weight desc'
    r = db.engine.execute(sql, (tid,)).fetchall()
    return dict(row for row in r[:max(len(r)/10, 5)])


@app.route('/rest/topics/<int:topic_id>')
def topic(topic_id):
    res = {'tid': topic_id, 'name': 'Topic %d'% topic_id}
    # res['data'] = mock_topic_keywords(topic_id)
    res['data'] = topic_keywords(topic_id)
    res['length'] = 1
    return json.dumps(res)


@app.route('/rest/events/<int:event_id>')
def event(event_id):
    sql = ''' select period_start, group_concat(keyword order by keyword_weight separator ','),
                group_concat(distinct topic_id order by topic_id separator ',')
                 from event where event_id=%s group by period_start;'''

    # res = db.engine.execute(sql, (event_id,)).fetchall()
    res = {'eid': event_id, 'name': 'Event %d'% event_id}
    tids = mock_event_tids(event_id)
    data = []
    for tid in tids:
        data.append({'tid':tid, 'name': 'Topic %d'%tid, 'values': mock_topic_hotness(tid, event_id)})

    res['length'] = len(tids)
    res['data'] = data

    return json.dumps(res)


@app.route('/rest/events')
def events():
    data = []
    for eid in mock_all_eids():
        data.append({'eid': eid, 'name':'Event %s'%eid, 'values': mock_event_weights(eid)})
    res = {'date_begin': '2012-12-01', 'length': MOCK_DATA_LENGTH, 'data': data}
    return json.dumps(res)


def query_topics():
    sql = '''select topic_id, period_start, topic_weight,
                group_concat(keyword order by keyword_weight separator ',')
                from topic
                group by topic_id,period_start
                order by period_start,topic_weight desc;'''
    r = db.engine.execute(sql).fetchall()

    topics_by_day = []
    last_pstart = None
    topics_list = []

    for tid, pstart, tweight, keywords_all_str in r:
        if last_pstart and pstart!=last_pstart:
            topics_by_day.append({'date':last_pstart.isoformat(), 'topics':topics_list[:20]})
            topics_list = []
        name = 'topic %d'%tid
        keywords = keywords_all_str.split(',')[:10]
        topic = {'name':name, 'keywords':keywords, 'weight':tweight}
        topics_list.append(topic)
        last_pstart = pstart
        # print tid, pstart, tweight, keywords

    return topics_by_day


def pivot_topic(topics_by_day):
    topics = defaultdict(list)
    for topics_of_day in topics_by_day:
        day = topics_of_day['date']
        topics_list = topics_of_day['topics']
        for topic in topics_list:
            topics[topic['name']].append((day, topic['weight']))
    return topics


def query_groups():
    sql = ''' select gid, group_concat(hashtag separator ',') from  hashtag_group_50 group by gid'''
    groups = db.engine.execute(sql).fetchall()
    g = []
    for t in groups:
        hash_in_g = t[1].split(',')
        g.append({'name':t[0], 'values':hash_in_g})
    return g


def query_g_hashes():
    sql = 'select distinct hashtag from hashtag_group_50'
    r = db.engine.execute(sql).fetchall()
    return [t[0] for t in r]


def fill_with_0(r, all_names):
    res = defaultdict(list)
    last_d = None
    cur_names = set()
    for t in r:
        name,cur_d,value = t
        # print last_d
        if last_d and last_d!=cur_d:
            for t in all_names:
                if t not in cur_names:
                    res[t].append(0)
            cur_names = set()
        if name in all_names:
            res[name].append(value)
            cur_names.add(name)
        last_d=cur_d
    return [ {'name':k, 'values':v} for k,v in res.items()]

def query_hashtags():
    sql = '''select hashtag, period_start, cnt from  hot_hashtag_detail where cnt>=100 order by period_start'''
    r = db.engine.execute(sql).fetchall()

    sql = ''' select hashtag from  hot_hashtag_detail group by hashtag having sum(cnt)>5000'''
    all_tags = db.engine.execute(sql).fetchall()
    all_tags_set = set(t[0] for t in all_tags)
    print len(r)
    return fill_with_0(r, all_tags_set)

def query_keywords(hashtag):
    sql = '''select keyword, period_start, keyword_weight from hashtag_keyword_50
            where hashtag=%s and keyword_weight>50 order by period_start'''
    r = db.engine.execute(sql, (hashtag,)).fetchall()
    print len(r)
    sql = ''' select keyword from hashtag_keyword_50 where hashtag=%s
                group by keyword  order by sum(keyword_weight) desc limit 20;'''

    all_keywords = db.engine.execute(sql, (hashtag,)).fetchall()
    all_keywords_set = set(t[0] for t in all_keywords)
    return fill_with_0(r, all_keywords_set)
    return [ {'name':kw, 'values':[len(kw)]*30} for kw in ['abc','123', 'hello-world']]


if __name__ == '__main__':
    app.debug = True
    app.run('0.0.0.0')
