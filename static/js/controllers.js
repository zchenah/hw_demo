//'use strict';

/* Controllers */

function plot(container, options){
    $('#'+container).highcharts({
            chart: { zoomType: 'x', spacingRight: 20, type: 'area'},
            title: { text: options.title },
            subtitle: { text: options.subtitle },
            xAxis: {
                type: 'datetime',
                maxZoom: 10 * 24 * 3600000,
                tickmarkPlacement: 'on',
                title: { enabled: false }
            },
            yAxis: {
                title: { text: 'Percent' }
            },
            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b> ({point.y:,.0f} millions)<br/>',
                shared: true
            },
            plotOptions: {
                area: {
                    stacking: options.stacking,
                    lineColor: '#ffffff',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#ffffff'
                    }
                },
                series: {
                    events: {
                        click:options.click
                    }
                }
            },
            series:  (function(series){
                console.log(series);
                var res = [];
                for (var i = 0; i < series.length; i++){
                    var data = series[i];
                    var d = {name:data['name'],
                        pointInterval:24*3600000,
                        pointStart:Date.UTC(2013, -1, 1),
                        data:data['values']}
                    res.push(d);
                }
                return res;
            })(options.series)
        });

}


function HashCtrl($scope, $resource, Topic, Hashtag, Keywords) {
    $scope.plot_keywords = function(hashtag){
        $scope.$apply(function(){
            Keywords.query({'topic':hashtag}, function(keywords ){
                console.log('keywords queried', hashtag);
                plot("keyword_chart", {title: 'Keyword of the topic '+hashtag+' on Twitter',
                                    subtitle: 'Author: HKUST',
                                    stacking: 'normal',
                                    series:keywords});

            });
        });
    }
    Hashtag.query({}, function(hashtags ){
        console.log('hashtags queried');
        plot("hash_chart", {title: 'Simple Topics on Twitter',
                            subtitle: 'Author: HKUST',
                            stacking: 'percent',
                            series:hashtags,
                            click:function(event){
                                var seriesIndex = this.index;
                                var topicName = this.name;
    //                            alert('clicked'+seriesIndex + topicName);
                                $scope.plot_keywords(topicName);
                            } });
    });
};


function TopicCtrl($scope, $resource, Topic, Hashtag, Keywords, Group) {
       $scope.plot_keywords = function(hashtag){
        $scope.$apply(function(){
            Keywords.query({'topic':hashtag}, function(keywords ){
                console.log('keywords queried', hashtag);
                plot("keyword_chart", {title: 'Keyword of the topic '+hashtag+' on Twitter',
                                    subtitle: 'Author: HKUST',
                                    stacking: 'normal',
                                    series:keywords});

            });
        });
    }

    var mix_topic = function(topics, callback){
        Group.query({}, function(groups){
            var res = {};
            for (var i=0; i<groups.lenght; i++){

            }

        })

    }

    Topic.query({}, function(topics ){
        console.log('topics queried');
        plot("topic_chart", {title: 'Combined Topics on Twitter',
                            subtitle: 'Author: HKUST',
                            stacking: 'percent',
                            series:topics,
                            click:function(event){
                                var seriesIndex = this.index;
                                var topicName = this.name;
    //                            alert('clicked'+seriesIndex + topicName);
                                $scope.plot_keywords(topicName);
                            } });
    });
};


//    $scope.plot_topics = function(topics){
//        $('#topics_chart').highcharts({
//            chart: { zoomType: 'x', spacingRight: 20, type: 'area'},
//            title: { text: 'Hot Topics on Twitter' },
//            subtitle: { text: 'Author: HKUST' },
//            xAxis: {
//                type: 'datetime',
//                maxZoom: 10 * 24 * 3600000,
//                tickmarkPlacement: 'on',
//                title: { enabled: false }
//            },
//            yAxis: {
//                title: { text: 'Percent' }
//            },
//            tooltip: {
//                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b> ({point.y:,.0f} tweets)<br/>',
//                shared: false
//            },
//            plotOptions: {
//                area: {
//                    stacking: 'percent',
//                    lineColor: '#ffffff',
//                    lineWidth: 1,
//                    marker: {
//                        lineWidth: 1,
//                        lineColor: '#ffffff'
//                    }
//                },
//                series: {
//                    events: {
//                        legendItemClick: function(event) {
//                            if (!this.visible)
//                                return true;
//                            var seriesIndex = this.index;
//                            var series = this.chart.series;
//                            for (var i = 0; i < series.length; i++)
//                            {
//                                if (series[i].index != seriesIndex)
//                                {
//                                    series[i].visible ? series[i].hide() : series[i].show();
//                                }
//                            }
//                            return false;
//                        },
//                        click: function(event) {
//                            var seriesIndex = this.index;
//                            var topicName = this.name;
////                            alert('clicked'+seriesIndex + topicName);
//                            $scope.plot_keywords(topicName);
//                        }
//                    }
//                }
//            },
//            series: (function(topics){
//                var res = [];
//                for (var i = 0; i < topics.length; i++){
////                    console.log(i);
////                    console.log(topic);
//                    var topic = topics[i];
//                    var d = {name:topic['name'],
//                        pointInterval:24*3600000,
//                        pointStart:Date.UTC(2013, 0, 1),
//                        data:topic['count']}
////                console.log(d);
//                    res.push(d);
//
//                }
//                return res;
//            })(topics)
//        });
//    };



//PhoneDetailCtrl.$inject = ['$scope', '$routeParams', 'Phone'];
