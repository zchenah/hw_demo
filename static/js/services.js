'use strict';

/* Services */

angular.module('hwDemoServices', ['ngResource']).
    factory('ChartData', function($resource){
        return $resource("./static/js/timeRangeChart.json?c=1", {}, {
            query: {method:'GET', params:{}, isArray:true}
        })
    }).factory('Topic', function($resource){
        return $resource("../rest/topics", {}, { });
    }).factory('Hashtag', function($resource){
        return $resource("../rest/hashtags", {}, { });
    }).factory('Keywords', function($resource){
        return $resource('../rest/topics/:tid', {}, {
            query: {method:'GET', params:{}, isArray:false}
        });
    }).factory('Group', function($resource){
        return $resource('../rest/groups', {}, { });
    }).factory('Events', function($resource){
        return $resource('../rest/events', {}, {
            query: {method:'GET', params:{}, isArray:false}
        });
    }).factory('Topics', function($resource){
        return $resource('../rest/events/:eid', {}, {
            query: {method:'GET', params:{}, isArray:false}
        });
    });

