//'use strict';

/* Controllers */

function plot(container, options){
    $('#'+container).highcharts({
            chart: { zoomType: 'x', spacingRight: 20, type: 'area'},
            title: { text: options.title },
            subtitle: { text: options.subtitle },
            xAxis: {
                type: 'datetime',
                maxZoom: 10 * 24 * 3600000,
                tickmarkPlacement: 'on',
                title: { enabled: false }
            },
            yAxis: {
                title: { text: options.yname }
            },
            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.percentage:.1f}%</b> <br/>',
                shared: true
            },
            plotOptions: {
                area: {
                    stacking: options.stacking,
                    lineColor: '#ffffff',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#ffffff'
                    }
                },
                series: {
                    events: {
                        click:options.click
                    }
                }
            },
            series: options.series
        });

}

function plot_pie(container, options){
    $('#'+container).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: options.title
        },
        tooltip: {
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Keywords proportion',
            data: options.series
//                [
//                ['Firefox',   45.0],
//                ['IE',       26.8],
//                {
//                    name: 'Chrome',
//                    y: 12.8,
//                    sliced: true,
//                    selected: true
//                },
//                ['Safari',    8.5],
//                ['Opera',     6.2],
//                ['Others',   0.7]
//            ]
        }]
    });
};

var series_format = function(rest_resp){
    var data_list = rest_resp['data'];
    console.log(data_list);
    var series = [];
    for (var i = 0; i < data_list.length; i++){
        var data = data_list[i];
//        console.log(data);
        var d = {name:data['name'],
            pointInterval:24*3600000,
            pointStart:Date.UTC(2013, -1, 1),
            data:data['values']}
//        console.log(d);
        series.push(d);
    }
    return series;
};

var pie_format = function(rest_resp){
    var data = rest_resp['data'];
    var res = []
    for (var k in data){
        res.push([k, data[k]]);
    }
    return res;
}

function EventsCtrl($scope, $timeout, Events, Topics, Keywords) {
    $scope.plot_keywords = function(tid){
        console.log('plot_keywords tid:', tid);
        $scope.$apply(function(){
            Keywords.query({'tid':tid}, function(keywords ){
                plot_pie("keywords_chart", {title: 'Keywords of the Topic '+tid+' ',
                    subtitle: 'Author: HKUST',
                    series: pie_format(keywords),
                    click: function(event){
//                        var seriesIndex = this.index;
//                        var tid = topics['data'][seriesIndex]['tid'];
//                        $scope.plot_keywords(tid);
                    } } );
            });
        });
    };

    $scope.plot_topics = function(eid){
        console.log('plot_topics eid:', eid);
        $scope.$apply(function(){
            Topics.query({'eid':eid}, function(topics ){
                plot("topics_chart", {title: 'Topics of the event '+eid+' ',
                                    subtitle: '',
                                    yname: 'Tweets',
                                    stacking: 'stack',
                                    series: series_format(topics),
                                    click: function(event){
                                        var seriesIndex = this.index;
                                        var tid = topics['data'][seriesIndex]['tid'];
                                        $scope.plot_keywords(tid);
                                    } } );
                $timeout(function(){ $scope.plot_keywords(topics['data'][0]['tid']);});
            });
        });
//        $scope.plot_keywords(0);
    }


    console.log('EventsCtrl');
    Events.query({}, function(events ){
        console.log('events queried');
        plot("events_chart", {title: 'Events on the Twitter Stream of Dec. 2012',
                                subtitle: 'Author: HKUST',
                                stacking: 'percent',
                                yname: 'Percent',
                                series: series_format(events),
                                click:function(event){
                                    var eid = events['data'][this.index]['eid']
                                    $scope.plot_topics(eid);
                                }
        });
    });
    $timeout(function(){ $scope.plot_topics(3);});
};


